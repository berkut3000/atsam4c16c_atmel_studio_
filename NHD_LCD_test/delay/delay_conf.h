/*
 * 
 _conf.h
 *
 * Created: 24/01/2018 08:22:19 a.m.
 *  Author: hydro
 */ 


#ifndef DELAY_CONF_H_
#define DELAY_CONF_H_

#include "stdio.h"

void Delay_N_ms(uint16_t val);
void Delay_N_us(uint16_t val);




#endif /* DELAY_CONF_H_ */